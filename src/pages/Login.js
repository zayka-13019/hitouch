import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import InputBase from '@material-ui/core/InputBase';
import Button from '@material-ui/core/Button';
import {makeStyles, useTheme, styled} from '@material-ui/core/styles';
import classNames from 'classnames';
import { grey } from '@material-ui/core/colors';

const drawerWidth = 227;

const colorWhite = '#ffffff';
const colorRed = '#FE2948';
const colorBgInput = grey[50];
const colorText = '#173145';
const colorBgRoot = '#E5E5E5';
const colorLblText = '#4A5964';

const CssInput = styled(InputBase)({
    '& .MuiInputBase-input' : {
        padding: 0,
    },
    '&:before': {
        borderBottom: 'none',
    },
});

const SignInButton = styled(Button)({
    transition: 'all .5s',
    '&:hover': {
        backgroundColor: colorRed,
    },
});

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        backgroundColor: colorBgRoot,
    },
    nav: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    drawerPaper: {
        width: drawerWidth,
        borderRight: 'none',
    },
    logo: {
        padding: '10px 0',
        textAlign: 'center',
        borderBottom: '0.5px solid #DCDCDC',
    },
    account: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        paddingTop: 247,
        height: '100vh',
        margin: '0 auto',
    },
    box: {
        width: '590px',
        padding: '56px 40px 60px',
        margin: '0 auto',
        borderRadius: 6,
        backgroundColor: colorWhite,
    },
    title: {
        fontWeight: 300,
        fontSize: 36,
        lineHeight: 1,
        color: colorText,
    },
    lblText: {
        marginBottom: 7,
        fontWeight: 300,
        fontSize: 12,
        lineHeight: 1.2,
        color: colorLblText,
    },
    loginTitle: {
        marginBottom: 30,
    },
    loginInput: {
        width: '100%',
        padding: '6.5px 10px',
        fontWeight: 300,
        fontSize: 12,
        lineHeight: 1.2,
        border: '0.5px solid #A9A9A9',
        borderRadius: 3,
        color: colorText,
        backgroundColor: colorBgInput,
    },
    loginUsername: {
        marginBottom: 26,
    },
    loginPassword: {
        marginBottom: 14,
    },
    loginLink: {
        display: 'block',
        marginBottom: 54,
        textAlign: 'right',
        fontWeight: 400,
        fontSize: 14,
        lineHeight: 1.2,
        color: colorRed,
        cursor: "pointer",
    },
    btn : {
        width: '100%',
        padding: '16px 13px 13px',
        textAlign: 'center',
        fontWeight: 500,
        fontSize: 16,
        lineHeight: 1.2,
        borderRadius: 6,
        border: 'none',
        color: colorWhite,
        backgroundColor: colorRed,
        boxShadow: 'none',
        cursor: 'pointer',
    },
}));

function ResponsiveDrawer() {
    const classes = useStyles();
    const theme = useTheme();

    return (
        <main className={classes.root}>
            <CssBaseline/>
            <nav className={classes.nav} aria-label="mailbox folders">
                <Drawer
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    variant="permanent"

                >
                    <Link className={classes.logo} href="#">
                        <img src='./logo.png' alt='logo'/>
                    </Link>
                </Drawer>
            </nav>
            <box className={classes.account}>
                <form className={classes.box} noValidate>
                    <Typography
                        className={classNames(classes.title, classes.loginTitle)}
                        component="h2">
                        Login
                    </Typography>
                    <InputLabel className={classes.lblText} htmlFor="username">
                        Username
                    </InputLabel>
                    <CssInput
                        className={classNames(classes.loginInput, classes.loginUsername)}
                        margin="none"
                        required
                        fullWidth
                        name="username"
                        placeholder="Email or Phone Number"
                        type="text"
                        id="username"
                        autoComplete="username"
                    />
                    <InputLabel className={classes.lblText} htmlFor="password">
                        Password
                    </InputLabel>
                    <CssInput
                        className={classNames(classes.loginInput, classes.loginPassword)}
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        placeholder="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                    />
                    <Link className={classes.loginLink} href="#" variant="body2">
                        Forgot Password
                    </Link>
                    <SignInButton
                        type="submit"
                        fullWidth
                        variant="contained"
                        className={classes.btn}
                    >
                        Sign In
                    </SignInButton>
                </form>
            </box>
        </main>
    );
}

export default ResponsiveDrawer;