import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import InputBase from '@material-ui/core/InputBase';
import Button from '@material-ui/core/Button';
import {makeStyles, useTheme, styled} from '@material-ui/core/styles';
import classNames from 'classnames';
import {grey} from '@material-ui/core/colors';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ButtonBase from "@material-ui/core/ButtonBase";
import ListItemText from "@material-ui/core/ListItemText";
import {Box} from "@material-ui/core";

const drawerWidth = 227;

const colorWhite = '#ffffff';
const colorRed = '#FE2948';
const colorBgInput = grey[50];
const colorText = '#173145';
const colorBgRoot = '#E5E5E5';
const colorLblText = '#4A5964';
const colorGreen = '#34C759';

const CssListItemText = styled(ListItemText)({    
    '& .MuiTypography-body1': {
        fontFamily: 'Roboto, sans-serif',
        fontWeight: 300,
        fontSize: 18,
        lineHeight: 1,    
        color: colorText,
        transition: 'all .5s',
        '&:hover': {
            fontWeight: 500,
        },
    },  
});

const CssMenuItem = styled(ButtonBase)({
    '&:hover': {
        backgroundColor: '#ffff',
    },
});

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        backgroundColor: colorBgRoot,
        
    },
    nav: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    drawerPaper: {
        width: drawerWidth,
        borderRight: 'none',
    },
    logo: {
        padding: '10px 0',
        textAlign: 'center',
        borderBottom: '0.5px solid #DCDCDC',
    },
    box: {
        width: '590px',
        padding: '56px 40px 60px',
        margin: '0 auto',
        borderRadius: 6,
        backgroundColor: colorWhite,
    },
    title: {
        fontWeight: 300,
        fontSize: 36,
        lineHeight: 1,
        color: colorText,
    },
    lblText: {
        marginBottom: 7,
        fontWeight: 300,
        fontSize: 12,
        lineHeight: 1.2,
        color: colorLblText,
    },
    loginTitle: {
        marginBottom: 30,
    },
    loginInput: {
        width: '100%',
        padding: '6.5px 10px',
        fontWeight: 300,
        fontSize: 12,
        lineHeight: 1.2,
        border: '0.5px solid #A9A9A9',
        borderRadius: 3,
        color: colorText,
        backgroundColor: colorBgInput,
    },
    loginUsername: {
        marginBottom: 26,
    },
    loginPassword: {
        marginBottom: 14,
    },
    loginLink: {
        display: 'block',
        marginBottom: 54,
        textAlign: 'right',
        fontWeight: 400,
        fontSize: 14,
        lineHeight: 1.2,
        color: colorRed,
        cursor: "pointer",
    },
    btn: {
        width: '100%',
        padding: '16px 13px 13px',
        textAlign: 'center',
        fontWeight: 500,
        fontSize: 16,
        lineHeight: 1.2,
        borderRadius: 6,
        border: 'none',
        color: colorWhite,
        backgroundColor: colorRed,
        boxShadow: 'none',
        cursor: 'pointer',
    },
    menu: {
        padding: '45px 30px',
    },
    menuBorderBottom: {
        borderBottom: '0.5px solid #DCDCDC',
    },
    list: {
        padding: 0,  
       '& + &' : {
           marginTop: 30,
       },     
    },    
    listMargin: {
        '& + &' : {
            marginTop: 26,
        },
    },
    listIcon: {
        marginRight: 10,
        minWidth: 0,
    },
    listText : {
        margin: 0,
    },
    listView: {
        padding: '70px 80px 0',
        height: '100vh',
        width: '100%'
    },
    listViewInner: {
        display: "flex",
        justifyContent: 'space-between',
        alignItems: 'center',
        
    },
    listViewLink: {
        color: colorWhite,
        backgroundColor: colorGreen,
        padding: '16px 25px 16px 21px',
        borderRadius: 6,
        fontWeight: 500,
        fontSize: 14,
        lineHeight: 1.2,
    },
}));

function ResponsiveDrawer() {
    const classes = useStyles();
    const theme = useTheme();

    return (
        <Box className={classes.root}>
            <CssBaseline/>
            <nav className={classes.nav} aria-label="mailbox folders">
                <Drawer
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    variant="permanent"

                >
                    <Link className={classes.logo} href="#">
                        <img src='./logo.png' alt='logo'/>
                    </Link>
                    <MenuList className={classNames(classes.menu, classes.menuBorderBottom)}>
                        <CssMenuItem className={classes.list}>
                            <ListItemIcon className={classes.listIcon}>
                                <svg width="16" height="16" viewBox="0 0 16 16">
                                    <path
                                        d="M8.42293 0L16 7.35341L15.1541 8.25357L13.9509 7.09224V15.3661L13.3552 16H9.78109L9.18541 15.3661V10.9287H6.80268V15.3661L6.207 16H2.63291L2.03723 15.3661V7.10238L0.845868 8.25357L0 7.35341L7.56515 0H8.42293ZM3.22859 5.94992V14.7322H5.61132V10.2948L6.207 9.66086H9.78109L10.3768 10.2948V14.7322H12.7595V5.94231L7.99404 1.3439L3.22859 5.94992Z"/>
                                </svg>
                            </ListItemIcon>
                            <CssListItemText className={classes.listText}>
                                Sessions
                            </CssListItemText>
                        </CssMenuItem>
                        <CssMenuItem className={classes.list}>
                            <ListItemIcon className={classes.listIcon}>
                                <svg width="16" height="16" viewBox="0 0 16 16">
                                    <path
                                        d="M1.14286 0H0V14.8571C0 15.1602 0.120408 15.4509 0.334735 15.6653C0.549062 15.8796 0.839753 16 1.14286 16H16V14.8571H1.14286V0Z"/>
                                    <path
                                        d="M15.9999 4H11.9999V5.14286H14.0514L9.71422 9.48L7.26279 7.02286C7.20967 6.9693 7.14647 6.92679 7.07683 6.89778C7.0072 6.86877 6.93251 6.85383 6.85707 6.85383C6.78164 6.85383 6.70695 6.86877 6.63732 6.89778C6.56768 6.92679 6.50448 6.9693 6.45136 7.02286L2.28564 11.1943L3.09136 12L6.85707 8.23429L9.3085 10.6914C9.36162 10.745 9.42482 10.7875 9.49446 10.8165C9.56409 10.8455 9.63878 10.8605 9.71422 10.8605C9.78965 10.8605 9.86434 10.8455 9.93397 10.8165C10.0036 10.7875 10.0668 10.745 10.1199 10.6914L14.8571 5.94857V8H15.9999V4Z"/>
                                </svg>
                            </ListItemIcon>
                            <CssListItemText className={classes.listText}>
                                Analytics
                            </CssListItemText>
                        </CssMenuItem>
                        <CssMenuItem className={classes.list}>
                            <ListItemIcon className={classes.listIcon}>
                                <svg width="16" height="16" viewBox="0 0 16 16">
                                    <path
                                        d="M7.99942 4.75342C7.57315 4.75342 7.15105 4.83738 6.75723 5.0005C6.3634 5.16363 6.00557 5.40273 5.70415 5.70415C5.40273 6.00557 5.16363 6.3634 5.00051 6.75723C4.83738 7.15105 4.75342 7.57315 4.75342 7.99942C4.75342 8.42569 4.83738 8.84779 5.00051 9.24161C5.16363 9.63543 5.40273 9.99327 5.70415 10.2947C6.00557 10.5961 6.3634 10.8352 6.75723 10.9983C7.15105 11.1615 7.57315 11.2454 7.99942 11.2454C8.86031 11.2454 9.68594 10.9034 10.2947 10.2947C10.9034 9.68594 11.2454 8.86031 11.2454 7.99942C11.2454 7.13853 10.9034 6.31289 10.2947 5.70415C9.68594 5.09541 8.86031 4.75342 7.99942 4.75342ZM5.75342 7.99942C5.75342 7.40374 5.99005 6.83246 6.41126 6.41126C6.83246 5.99005 7.40374 5.75342 7.99942 5.75342C8.59509 5.75342 9.16637 5.99005 9.58758 6.41126C10.0088 6.83246 10.2454 7.40374 10.2454 7.99942C10.2454 8.59509 10.0088 9.16637 9.58758 9.58758C9.16637 10.0088 8.59509 10.2454 7.99942 10.2454C7.40374 10.2454 6.83246 10.0088 6.41126 9.58758C5.99005 9.16637 5.75342 8.59509 5.75342 7.99942Z"/>
                                    <path
                                        d="M9.7955 1.3425C9.2685 -0.4475 6.7305 -0.4475 6.2035 1.3425L6.1095 1.6615C6.0727 1.78645 6.0084 1.90158 5.92132 1.99846C5.83424 2.09533 5.72658 2.17149 5.60624 2.22135C5.4859 2.27121 5.35593 2.29351 5.22585 2.28662C5.09578 2.27972 4.96889 2.24381 4.8545 2.1815L4.5625 2.0215C2.9225 1.1295 1.1295 2.9235 2.0225 4.5625L2.1815 4.8545C2.24381 4.96889 2.27972 5.09578 2.28662 5.22585C2.29351 5.35593 2.27121 5.4859 2.22135 5.60624C2.17149 5.72658 2.09533 5.83424 1.99846 5.92132C1.90158 6.0084 1.78645 6.0727 1.6615 6.1095L1.3425 6.2035C-0.4475 6.7305 -0.4475 9.2685 1.3425 9.7955L1.6615 9.8895C1.78645 9.9263 1.90158 9.9906 1.99846 10.0777C2.09533 10.1648 2.17149 10.2724 2.22135 10.3928C2.27121 10.5131 2.29351 10.6431 2.28662 10.7731C2.27972 10.9032 2.24381 11.0301 2.1815 11.1445L2.0215 11.4365C1.1295 13.0765 2.9225 14.8705 4.5625 13.9765L4.8545 13.8175C4.96889 13.7552 5.09578 13.7193 5.22585 13.7124C5.35593 13.7055 5.4859 13.7278 5.60624 13.7776C5.72658 13.8275 5.83424 13.9037 5.92132 14.0005C6.0084 14.0974 6.0727 14.2125 6.1095 14.3375L6.2035 14.6565C6.7305 16.4465 9.2685 16.4465 9.7955 14.6565L9.8895 14.3375C9.9263 14.2125 9.9906 14.0974 10.0777 14.0005C10.1648 13.9037 10.2724 13.8275 10.3928 13.7776C10.5131 13.7278 10.6431 13.7055 10.7731 13.7124C10.9032 13.7193 11.0301 13.7552 11.1445 13.8175L11.4365 13.9775C13.0765 14.8705 14.8705 13.0755 13.9765 11.4365L13.8175 11.1445C13.7552 11.0301 13.7193 10.9032 13.7124 10.7731C13.7055 10.6431 13.7278 10.5131 13.7776 10.3928C13.8275 10.2724 13.9037 10.1648 14.0005 10.0777C14.0974 9.9906 14.2125 9.9263 14.3375 9.8895L14.6565 9.7955C16.4465 9.2685 16.4465 6.7305 14.6565 6.2035L14.3375 6.1095C14.2125 6.0727 14.0974 6.0084 14.0005 5.92132C13.9037 5.83424 13.8275 5.72658 13.7776 5.60624C13.7278 5.4859 13.7055 5.35593 13.7124 5.22585C13.7193 5.09578 13.7552 4.96889 13.8175 4.8545L13.9775 4.5625C14.8705 2.9225 13.0755 1.1295 11.4365 2.0225L11.1445 2.1815C11.0301 2.24381 10.9032 2.27972 10.7731 2.28662C10.6431 2.29351 10.5131 2.27121 10.3928 2.22135C10.2724 2.17149 10.1648 2.09533 10.0777 1.99846C9.9906 1.90158 9.9263 1.78645 9.8895 1.6615L9.7955 1.3425ZM7.1625 1.6255C7.4085 0.7905 8.5905 0.7905 8.8365 1.6255L8.9305 1.9445C9.00952 2.21259 9.14754 2.45959 9.33443 2.6674C9.52132 2.87521 9.75235 3.03856 10.0106 3.14548C10.2688 3.2524 10.5477 3.30017 10.8268 3.28529C11.1059 3.27041 11.3781 3.19326 11.6235 3.0595L11.9145 2.8995C12.6785 2.4845 13.5145 3.3195 13.0985 4.0845L12.9395 4.3765C12.8059 4.62193 12.729 4.89413 12.7143 5.17316C12.6995 5.45218 12.7474 5.73097 12.8544 5.98908C12.9615 6.2472 13.1249 6.4781 13.3327 6.66488C13.5405 6.85165 13.7875 6.98956 14.0555 7.0685L14.3735 7.1625C15.2085 7.4085 15.2085 8.5905 14.3735 8.8365L14.0545 8.9305C13.7864 9.00952 13.5394 9.14754 13.3316 9.33443C13.1238 9.52132 12.9604 9.75235 12.8535 10.0106C12.7466 10.2688 12.6988 10.5477 12.7137 10.8268C12.7286 11.1059 12.8057 11.3781 12.9395 11.6235L13.0995 11.9145C13.5145 12.6785 12.6795 13.5145 11.9145 13.0985L11.6235 12.9395C11.378 12.8057 11.1057 12.7286 10.8266 12.7138C10.5474 12.699 10.2685 12.7469 10.0103 12.8539C9.75204 12.9609 9.52102 13.1244 9.33418 13.3323C9.14734 13.5402 9.00941 13.7873 8.9305 14.0555L8.8365 14.3735C8.5905 15.2085 7.4085 15.2085 7.1625 14.3735L7.0685 14.0545C6.98944 13.7866 6.85145 13.5397 6.66463 13.332C6.47781 13.1243 6.24689 12.961 5.98879 12.8541C5.73069 12.7472 5.45194 12.6994 5.17297 12.7141C4.894 12.7289 4.62186 12.8059 4.3765 12.9395L4.0845 13.0995C3.3205 13.5145 2.4845 12.6795 2.9005 11.9145L3.0595 11.6235C3.19345 11.3781 3.27075 11.1057 3.28573 10.8265C3.3007 10.5473 3.25297 10.2683 3.14604 10.0099C3.03911 9.75158 2.87569 9.52044 2.66778 9.33348C2.45986 9.14652 2.21272 9.00849 1.9445 8.9295L1.6255 8.8355C0.7905 8.5895 0.7905 7.4075 1.6255 7.1615L1.9445 7.0675C2.21231 6.98841 2.45904 6.85044 2.66664 6.66369C2.87425 6.47694 3.03746 6.24613 3.14435 5.98816C3.25124 5.73018 3.2991 5.45157 3.28442 5.17272C3.26973 4.89387 3.19289 4.62182 3.0595 4.3765L2.8995 4.0845C2.4845 3.3205 3.3195 2.4845 4.0845 2.9005L4.3765 3.0595C4.62186 3.19308 4.894 3.27007 5.17297 3.28485C5.45194 3.29963 5.73069 3.25182 5.98879 3.14492C6.24689 3.03801 6.47781 2.87473 6.66463 2.66702C6.85145 2.45931 6.98944 2.21245 7.0685 1.9445L7.1625 1.6255Z"/>
                                </svg>
                            </ListItemIcon>
                            <CssListItemText className={classes.listText}>
                                Settings
                            </CssListItemText>
                        </CssMenuItem>
                    </MenuList>
                    <MenuList className={classNames(classes.menu, classes.menuBorderBottom)}>
                        <CssMenuItem className={classNames(classes.list, classes.listMargin)}>
                            <CssListItemText className={classes.listText}>
                                Help
                            </CssListItemText>
                        </CssMenuItem>
                        <MenuItem className={classNames(classes.list, classes.listMargin)}>
                            <CssListItemText className={classes.listText}>
                                Contact Us
                            </CssListItemText>
                        </MenuItem>
                    </MenuList>
                    <MenuList className={classes.menu}>
                        <CssMenuItem className={classes.list}>
                            <CssListItemText className={classes.listText}>
                                Sign Out
                            </CssListItemText>
                        </CssMenuItem>
                    </MenuList>
                </Drawer>
            </nav>

            <section className={classes.listView}>
                <div className={classes.listViewInner}>
                    <Typography variant="h1" component="h1" className={classNames(classes.title, )}>
                        Sessions
                    </Typography>
                    <Link className={classes.listViewLink} href="#">
                        Create Session
                    </Link>
                </div>
            </section>
        </Box>
    );
}

export default ResponsiveDrawer;